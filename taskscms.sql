-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2018 at 05:55 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskscms`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'public', NULL, NULL),
(2, 'private', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'cat1', '2018-07-29 09:13:01', '2018-07-29 09:13:01'),
(2, 'cat2', '2018-07-29 09:13:08', '2018-07-29 09:13:08'),
(3, 'cat3', '2018-07-29 09:13:15', '2018-07-29 09:13:15'),
(4, 'cat4', '2018-07-29 09:13:21', '2018-07-29 09:13:21'),
(5, 'cat5', '2018-07-29 09:13:26', '2018-07-29 09:13:26'),
(6, 'cat6', '2018-07-29 09:13:33', '2018-07-29 09:13:33'),
(7, 'cat7', '2018-07-29 09:13:42', '2018-07-29 09:13:42'),
(8, 'cat8', '2018-07-29 09:13:49', '2018-07-29 09:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `description`, `created_by`, `task_id`, `created_at`, `updated_at`) VALUES
(1, 'doing cool', 1, 1, '2018-07-29 09:28:30', '2018-07-29 09:28:30'),
(2, 'Almost done', 1, 4, '2018-07-29 09:28:57', '2018-07-29 09:28:57'),
(3, 'powerful', 1, 1, '2018-07-29 10:03:07', '2018-07-29 10:03:07'),
(4, 'to be finished in a weeks time.', 1, 1, '2018-07-29 10:06:32', '2018-07-29 10:06:32'),
(5, 'This is marvelous', 1, 2, '2018-07-31 08:20:33', '2018-07-31 08:20:33'),
(6, 'Excellent Stuff!', 1, 2, '2018-07-31 08:21:34', '2018-07-31 08:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'd1', '2018-07-29 09:13:59', '2018-07-29 09:13:59'),
(2, 'd2', '2018-07-29 09:14:05', '2018-07-29 09:14:05'),
(3, 'd3', '2018-07-29 09:14:11', '2018-07-29 09:14:11'),
(4, 'd4', '2018-07-29 09:14:17', '2018-07-29 09:14:17'),
(5, 'd5', '2018-07-29 09:14:21', '2018-07-29 09:14:21'),
(6, 'd6', '2018-07-29 09:14:29', '2018-07-29 09:14:29'),
(7, 'd7', '2018-07-29 09:14:38', '2018-07-29 09:14:38'),
(8, 'd8', '2018-07-29 09:14:44', '2018-07-29 09:14:44');

-- --------------------------------------------------------

--
-- Table structure for table `department_task`
--

CREATE TABLE `department_task` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department_task`
--

INSERT INTO `department_task` (`id`, `department_id`, `task_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL),
(2, 7, 1, NULL, NULL),
(3, 1, 2, NULL, NULL),
(4, 2, 2, NULL, NULL),
(5, 5, 3, NULL, NULL),
(6, 5, 4, NULL, NULL),
(7, 8, 4, NULL, NULL),
(8, 4, 5, NULL, NULL),
(9, 8, 5, NULL, NULL),
(10, 2, 6, NULL, NULL),
(11, 4, 6, NULL, NULL),
(12, 1, 7, NULL, NULL),
(13, 6, 7, NULL, NULL),
(14, 6, 8, NULL, NULL),
(15, 4, 9, NULL, NULL),
(16, 5, 9, NULL, NULL),
(17, 4, 10, NULL, NULL),
(18, 5, 10, NULL, NULL),
(19, 4, 11, NULL, NULL),
(20, 5, 11, NULL, NULL),
(21, 4, 12, NULL, NULL),
(22, 5, 12, NULL, NULL),
(23, 4, 13, NULL, NULL),
(24, 5, 13, NULL, NULL),
(25, 2, 14, NULL, NULL),
(26, 2, 15, NULL, NULL),
(27, 4, 16, NULL, NULL),
(28, 6, 16, NULL, NULL),
(29, 4, 17, NULL, NULL),
(30, 6, 17, NULL, NULL),
(31, 4, 18, NULL, NULL),
(32, 6, 18, NULL, NULL),
(33, 4, 19, NULL, NULL),
(34, 6, 19, NULL, NULL),
(35, 4, 20, NULL, NULL),
(36, 6, 20, NULL, NULL),
(37, 4, 21, NULL, NULL),
(38, 6, 21, NULL, NULL),
(39, 4, 22, NULL, NULL),
(40, 6, 22, NULL, NULL),
(41, 3, 23, NULL, NULL),
(42, 5, 23, NULL, NULL),
(43, 7, 23, NULL, NULL),
(44, 3, 24, NULL, NULL),
(45, 5, 24, NULL, NULL),
(46, 7, 24, NULL, NULL),
(47, 3, 25, NULL, NULL),
(48, 5, 25, NULL, NULL),
(49, 7, 25, NULL, NULL),
(50, 3, 26, NULL, NULL),
(51, 5, 26, NULL, NULL),
(52, 7, 26, NULL, NULL),
(53, 3, 27, NULL, NULL),
(54, 5, 27, NULL, NULL),
(55, 7, 27, NULL, NULL),
(56, 3, 28, NULL, NULL),
(57, 5, 28, NULL, NULL),
(58, 7, 28, NULL, NULL),
(59, 5, 30, NULL, NULL),
(60, 7, 30, NULL, NULL),
(61, 5, 31, NULL, NULL),
(62, 7, 31, NULL, NULL),
(63, 5, 32, NULL, NULL),
(64, 7, 32, NULL, NULL),
(65, 5, 33, NULL, NULL),
(66, 7, 33, NULL, NULL),
(67, 5, 34, NULL, NULL),
(68, 7, 34, NULL, NULL),
(69, 5, 35, NULL, NULL),
(70, 7, 35, NULL, NULL),
(71, 5, 36, NULL, NULL),
(72, 7, 36, NULL, NULL),
(73, 5, 37, NULL, NULL),
(74, 7, 37, NULL, NULL),
(75, 5, 38, NULL, NULL),
(76, 7, 38, NULL, NULL),
(77, 5, 39, NULL, NULL),
(78, 7, 39, NULL, NULL),
(79, 5, 40, NULL, NULL),
(80, 7, 40, NULL, NULL),
(81, 5, 41, NULL, NULL),
(82, 7, 41, NULL, NULL),
(83, 5, 42, NULL, NULL),
(84, 7, 42, NULL, NULL),
(85, 5, 43, NULL, NULL),
(86, 7, 43, NULL, NULL),
(87, 5, 44, NULL, NULL),
(88, 7, 44, NULL, NULL),
(89, 5, 45, NULL, NULL),
(90, 7, 45, NULL, NULL),
(91, 5, 46, NULL, NULL),
(92, 7, 46, NULL, NULL),
(93, 5, 47, NULL, NULL),
(94, 7, 47, NULL, NULL),
(95, 4, 48, NULL, NULL),
(96, 6, 48, NULL, NULL),
(97, 7, 48, NULL, NULL),
(98, 4, 49, NULL, NULL),
(99, 6, 49, NULL, NULL),
(100, 7, 49, NULL, NULL),
(101, 4, 50, NULL, NULL),
(102, 6, 50, NULL, NULL),
(103, 7, 50, NULL, NULL),
(104, 4, 51, NULL, NULL),
(105, 6, 51, NULL, NULL),
(106, 7, 51, NULL, NULL),
(107, 4, 52, NULL, NULL),
(108, 6, 52, NULL, NULL),
(109, 4, 53, NULL, NULL),
(110, 6, 53, NULL, NULL),
(111, 4, 54, NULL, NULL),
(112, 6, 54, NULL, NULL),
(113, 4, 55, NULL, NULL),
(114, 6, 55, NULL, NULL),
(115, 3, 56, NULL, NULL),
(116, 5, 56, NULL, NULL),
(117, 3, 57, NULL, NULL),
(118, 5, 57, NULL, NULL),
(119, 3, 58, NULL, NULL),
(120, 5, 58, NULL, NULL),
(121, 3, 59, NULL, NULL),
(122, 6, 59, NULL, NULL),
(123, 3, 60, NULL, NULL),
(124, 6, 60, NULL, NULL),
(125, 3, 61, NULL, NULL),
(126, 6, 61, NULL, NULL),
(127, 2, 62, NULL, NULL),
(128, 4, 62, NULL, NULL),
(129, 5, 62, NULL, NULL),
(130, 2, 63, NULL, NULL),
(131, 4, 63, NULL, NULL),
(132, 5, 63, NULL, NULL),
(133, 2, 64, NULL, NULL),
(134, 4, 64, NULL, NULL),
(135, 5, 64, NULL, NULL),
(136, 2, 65, NULL, NULL),
(137, 4, 65, NULL, NULL),
(138, 5, 65, NULL, NULL),
(139, 2, 66, NULL, NULL),
(140, 4, 66, NULL, NULL),
(141, 5, 66, NULL, NULL),
(142, 2, 67, NULL, NULL),
(143, 4, 67, NULL, NULL),
(144, 5, 67, NULL, NULL),
(145, 2, 68, NULL, NULL),
(146, 4, 68, NULL, NULL),
(147, 5, 68, NULL, NULL),
(148, 2, 69, NULL, NULL),
(149, 4, 69, NULL, NULL),
(150, 5, 69, NULL, NULL),
(151, 2, 70, NULL, NULL),
(152, 3, 70, NULL, NULL),
(153, 4, 70, NULL, NULL),
(154, 4, 71, NULL, NULL),
(155, 5, 71, NULL, NULL),
(156, 4, 72, NULL, NULL),
(157, 5, 72, NULL, NULL),
(158, 4, 73, NULL, NULL),
(159, 5, 73, NULL, NULL),
(160, 1, 74, NULL, NULL),
(161, 3, 74, NULL, NULL),
(162, 2, 75, NULL, NULL),
(163, 3, 75, NULL, NULL),
(164, 4, 75, NULL, NULL),
(165, 3, 76, NULL, NULL),
(166, 1, 77, NULL, NULL),
(167, 8, 78, NULL, NULL),
(168, 8, 79, NULL, NULL),
(169, 4, 80, NULL, NULL),
(170, 5, 81, NULL, NULL),
(171, 5, 82, NULL, NULL),
(172, 4, 83, NULL, NULL),
(173, 3, 84, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `follow_employees`
--

CREATE TABLE `follow_employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follow_employees`
--

INSERT INTO `follow_employees` (`id`, `employee_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-07-30 21:12:51', '2018-07-30 21:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `follow_tasks`
--

CREATE TABLE `follow_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `follow_tasks`
--

INSERT INTO `follow_tasks` (`id`, `user_id`, `task_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-07-30 21:03:20', '2018-07-30 21:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `job_levels`
--

CREATE TABLE `job_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_levels`
--

INSERT INTO `job_levels` (`id`, `level`, `created_at`, `updated_at`) VALUES
(1, 'intern', '2018-07-29 09:14:56', '2018-07-29 09:14:56'),
(2, 'beginner', '2018-07-29 09:15:07', '2018-07-29 09:15:07'),
(3, 'Intermediate', '2018-07-29 09:15:13', '2018-07-29 09:15:13'),
(4, 'consultant', '2018-07-29 09:15:23', '2018-07-29 09:15:23'),
(5, 'expert', '2018-07-29 09:15:30', '2018-07-29 09:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(134, '2018_07_28_021915_create_my_tasks_table', 1),
(136, '2014_10_12_000000_create_users_table', 2),
(137, '2014_10_12_100000_create_password_resets_table', 2),
(138, '2018_07_25_105755_create_departments_table', 2),
(139, '2018_07_25_105919_create_categories_table', 2),
(140, '2018_07_25_110221_create_tasks_table', 2),
(141, '2018_07_25_111405_create_job_levels_table', 2),
(142, '2018_07_25_124545_create_access_levels_table', 2),
(143, '2018_07_25_131245_create_task_user_table', 2),
(144, '2018_07_25_143049_create_follow_tasks_table', 2),
(145, '2018_07_25_143220_create_follow_employees_table', 2),
(146, '2018_07_26_133314_create_positions_table', 2),
(147, '2018_07_26_152724_create_priorities_table', 2),
(148, '2018_07_27_124713_create_department_task_table', 2),
(149, '2018_07_29_065116_create_comments_table', 2),
(150, '2018_07_30_143546_create_oauth_identities_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `my_tasks`
--

CREATE TABLE `my_tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `done` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_identities`
--

CREATE TABLE `oauth_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'junior', NULL, NULL),
(2, 'senior', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'super high', NULL, NULL),
(2, 'high', NULL, NULL),
(3, 'medium', NULL, NULL),
(4, 'low', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `due_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `assign_to` int(10) UNSIGNED NOT NULL,
  `done` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_level_id` int(10) UNSIGNED NOT NULL,
  `recur_days` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recur_daily` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_email_after` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `category_id`, `due_date`, `priority_id`, `description`, `created_by`, `assign_to`, `done`, `status`, `document`, `access_level_id`, `recur_days`, `recur_daily`, `send_email_after`, `created_at`, `updated_at`) VALUES
(1, 'Action point', 1, '07/12/2018', 1, 'great', 3, 1, 75, 'pending', '', 2, 'fridays', '', NULL, '2018-07-29 09:19:41', '2018-07-29 21:22:37'),
(2, 'Test', 4, '07/20/2018', 2, 'excellent buddy', 1, 6, 0, 'open', '', 2, NULL, '1', NULL, '2018-07-29 09:20:29', '2018-07-29 21:19:46'),
(3, 'Keep it', 2, '07/12/2018', 1, 'awesome', 2, 6, 0, 'open', '', 2, NULL, NULL, '5', '2018-07-29 09:21:10', '2018-07-30 08:25:44'),
(4, 'Excellent', 5, '07/28/2018', 4, 'asad', 1, 1, 75, 'started', '', 2, NULL, NULL, '2', '2018-07-29 09:21:37', '2018-07-29 21:19:52'),
(5, 'good', 4, '07/26/2018', 1, 'good staff', 3, 1, 0, 'open', '', 1, 'thursdays', '', NULL, '2018-07-29 09:23:26', '2018-07-29 09:23:26'),
(6, 'asdasd', 6, '07/26/2018', 3, 'great stuff', 1, 1, 0, 'open', '', 2, NULL, NULL, '4', '2018-07-29 09:33:37', '2018-07-29 09:33:37'),
(7, 'Wow', 1, '08/30/2018', 2, 'walala', 1, 2, 0, 'open', '', 2, NULL, NULL, '3', '2018-07-29 09:34:05', '2018-07-29 09:35:03'),
(8, 'adminTest', 5, '08/30/2018', 4, 'excellent wow', 1, 1, 0, 'open', '', 2, 'fridays', '', NULL, '2018-07-29 09:34:47', '2018-07-29 09:34:47'),
(9, 'Task7', 5, '07/26/2018', 4, 'tftfuy', 1, 3, 0, 'open', '', 2, NULL, NULL, '67', '2018-07-29 12:48:59', '2018-07-29 21:20:07'),
(10, 'Task5', 6, '07/26/2018', 3, 'ppmnjjh', 1, 3, 0, 'open', '', 2, NULL, NULL, '67', '2018-07-29 12:51:32', '2018-07-29 21:20:00'),
(11, 'Task5', 5, '07/26/2018', 4, 'tftfuy', 1, 3, 0, 'open', '', 2, NULL, NULL, '67', '2018-07-29 12:52:31', '2018-07-30 01:49:04'),
(14, 'new', 5, '07/17/2018', 1, 'njn', 1, 2, 0, 'open', '', 2, NULL, NULL, '9', '2018-07-29 12:55:13', '2018-07-29 12:55:13'),
(15, 'new', 5, '07/17/2018', 1, 'njn', 1, 2, 0, 'open', '', 2, NULL, NULL, '9', '2018-07-29 12:56:00', '2018-07-29 12:56:00'),
(16, 'Areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 2, NULL, NULL, '7', '2018-07-29 12:58:53', '2018-07-30 01:49:11'),
(17, 'areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 1, NULL, NULL, '7', '2018-07-29 12:59:55', '2018-07-29 12:59:55'),
(18, 'areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 1, NULL, NULL, '7', '2018-07-29 13:00:34', '2018-07-29 13:00:34'),
(19, 'areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 1, NULL, NULL, '7', '2018-07-29 13:01:00', '2018-07-29 13:01:00'),
(20, 'Areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 2, NULL, NULL, '7', '2018-07-29 13:03:12', '2018-07-29 21:20:27'),
(21, 'Areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 2, NULL, NULL, '7', '2018-07-29 13:05:16', '2018-07-29 21:20:18'),
(22, 'areawrwer', 6, '07/31/2018', 2, 'yourdtrd', 1, 5, 0, 'open', '', 1, NULL, NULL, '7', '2018-07-29 13:09:55', '2018-07-29 13:09:55'),
(23, 'tttt', 2, '07/18/2018', 3, 'scasdcd', 1, 4, 0, 'open', '', 1, 'thursdays', '', NULL, '2018-07-29 13:11:01', '2018-07-29 13:11:01'),
(24, 'tttt', 2, '07/18/2018', 3, 'scasdcd', 1, 4, 0, 'open', '', 1, 'thursdays', '', NULL, '2018-07-29 13:15:59', '2018-07-29 13:15:59'),
(25, 'Tttt', 2, '07/18/2018', 3, 'scasdcd', 1, 4, 0, 'open', '', 2, 'thursdays', '', NULL, '2018-07-29 13:16:20', '2018-07-30 01:48:45'),
(26, 'Tttt', 2, '07/18/2018', 3, 'scasdcd', 1, 4, 0, 'open', '', 2, 'thursdays', '', NULL, '2018-07-29 13:16:22', '2018-07-30 01:48:56'),
(27, 'tttt', 2, '07/18/2018', 3, 'scasdcd', 1, 4, 0, 'open', '', 1, 'thursdays', '', NULL, '2018-07-29 13:16:39', '2018-07-29 13:16:39'),
(28, 'Tttt', 2, '07/18/2018', 3, 'scasdcd', 2, 4, 0, 'open', '', 2, 'thursdays', '', NULL, '2018-07-29 13:16:50', '2018-07-29 21:19:38'),
(29, 'finance module', 1, '07/31/2018', 1, 'to meer deadline', 2, 5, 0, 'open', '', 1, NULL, '1', NULL, '2018-07-29 20:03:21', '2018-07-29 20:03:21'),
(30, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 2, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:14:29', '2018-07-29 20:14:29'),
(31, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:17:32', '2018-07-29 20:17:32'),
(32, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:17:54', '2018-07-29 20:17:54'),
(33, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:18:14', '2018-07-29 20:18:14'),
(34, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:21:15', '2018-07-29 20:21:15'),
(35, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:44:15', '2018-07-29 20:44:15'),
(36, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:51:55', '2018-07-29 20:51:55'),
(37, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:52:02', '2018-07-29 20:52:02'),
(38, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:52:44', '2018-07-29 20:52:44'),
(39, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 20:52:57', '2018-07-29 20:52:57'),
(40, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 21:01:19', '2018-07-29 21:01:19'),
(41, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 21:02:32', '2018-07-29 21:02:32'),
(42, 'wonder', 4, '07/31/2018', 2, 'wala', 2, 1, 0, 'open', '', 1, NULL, NULL, '8', '2018-07-29 21:05:51', '2018-07-29 21:05:51'),
(43, 'Wonder', 4, '07/31/2018', 2, 'wala', 3, 1, 0, 'open', '', 2, NULL, NULL, '8', '2018-07-29 21:05:57', '2018-07-29 21:20:55'),
(44, 'Wonder', 4, '07/31/2018', 2, 'wala', 3, 1, 0, 'open', '', 2, NULL, NULL, '8', '2018-07-29 21:05:59', '2018-07-29 21:20:45'),
(45, 'Wonder', 4, '07/31/2018', 2, 'wala', 3, 1, 0, 'open', '', 2, NULL, NULL, '8', '2018-07-29 21:08:58', '2018-07-29 21:21:04'),
(46, 'Wonder', 4, '07/31/2018', 2, 'wala', 3, 1, 0, 'open', '', 2, NULL, NULL, '8', '2018-07-29 21:09:32', '2018-07-29 21:21:18'),
(47, 'Wonder', 4, '07/31/2018', 2, 'wala', 3, 1, 0, 'open', '', 2, NULL, NULL, '8', '2018-07-29 21:09:48', '2018-07-29 21:21:31'),
(48, 'SaS', 1, '07/26/2018', 1, 'zxzx', 3, 4, 0, 'open', '', 1, NULL, '1', NULL, '2018-07-29 21:11:07', '2018-07-29 21:11:07'),
(49, 'SaS', 1, '07/26/2018', 1, 'zxzx', 3, 4, 0, 'open', '', 1, NULL, '1', NULL, '2018-07-29 21:11:08', '2018-07-29 21:11:08'),
(50, 'SaS', 1, '07/26/2018', 1, 'zxzx', 3, 4, 0, 'open', '', 1, NULL, '1', NULL, '2018-07-29 21:12:52', '2018-07-29 21:12:52'),
(51, 'SaS', 1, '07/26/2018', 1, 'zxzx', 3, 4, 0, 'open', '', 2, NULL, '1', NULL, '2018-07-29 21:13:22', '2018-07-29 21:22:16'),
(52, 'atetet', 1, '07/18/2018', 2, 'asxaz', 3, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:28:02', '2018-07-30 00:28:02'),
(53, 'atetet', 1, '07/18/2018', 2, 'asxaz', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:29:37', '2018-07-30 00:29:37'),
(54, 'atetet', 1, '07/18/2018', 2, 'asxaz', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:30:23', '2018-07-30 00:30:23'),
(55, 'atetet', 1, '07/18/2018', 2, 'asxaz', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:33:42', '2018-07-30 00:33:42'),
(56, 'action point3', 1, '07/27/2018', 3, 'great wow', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:36:04', '2018-07-30 00:36:04'),
(57, 'action point3', 1, '07/27/2018', 3, 'great wow', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:38:45', '2018-07-30 00:38:45'),
(58, 'action point3', 1, '07/27/2018', 3, 'great wow', 1, 4, 0, 'open', '', 1, NULL, NULL, '23', '2018-07-30 00:39:01', '2018-07-30 00:39:01'),
(59, 'luckily', 5, '07/26/2018', 2, 'assad', 2, 1, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 00:39:52', '2018-07-30 00:39:52'),
(60, 'luckily', 5, '07/26/2018', 2, 'assad', 2, 1, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 00:50:46', '2018-07-30 00:50:46'),
(61, 'luckily', 5, '07/26/2018', 2, 'assad', 1, 1, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 00:55:59', '2018-07-30 00:55:59'),
(62, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 00:57:14', '2018-07-30 00:57:14'),
(63, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 00:58:27', '2018-07-30 00:58:27'),
(64, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 00:59:28', '2018-07-30 00:59:28'),
(65, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:00:41', '2018-07-30 01:00:41'),
(66, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:01:02', '2018-07-30 01:01:02'),
(67, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:02:56', '2018-07-30 01:02:56'),
(68, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:03:19', '2018-07-30 01:03:19'),
(69, 'asdasd', 1, '07/27/2018', 2, 'ASAD', 1, 1, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:05:22', '2018-07-30 01:05:22'),
(70, 'awesome', 5, '07/26/2018', 2, 'this is cool buddy', 1, 4, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 01:06:22', '2018-07-30 01:06:22'),
(71, 'cool', 4, '07/31/2018', 3, 'cool staff', 1, 1, 0, 'open', '', 1, NULL, NULL, '9', '2018-07-30 01:07:37', '2018-07-30 01:07:37'),
(72, 'cool', 4, '07/31/2018', 3, 'cool staff', 1, 1, 0, 'open', '', 1, NULL, NULL, '9', '2018-07-30 01:08:28', '2018-07-30 01:08:28'),
(73, 'cool', 4, '07/31/2018', 3, 'cool staff', 1, 1, 0, 'open', '', 1, NULL, NULL, '9', '2018-07-30 01:13:32', '2018-07-30 01:13:32'),
(74, 'aewae', 1, '07/26/2018', 3, 'adasd', 1, 4, 0, 'open', '', 1, NULL, NULL, '4', '2018-07-30 01:50:38', '2018-07-30 01:50:38'),
(75, 'Djasrfh', 4, '07/26/2018', 2, 'uiwehr', 1, 4, 0, 'open', '', 1, NULL, '1', NULL, '2018-07-30 01:52:05', '2018-07-30 01:52:05'),
(76, 'new task', 3, '07/18/2018', 2, 'asxasxasx', 1, 3, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 09:45:45', '2018-07-30 09:45:45'),
(77, 'wrewe', 4, '07/25/2018', 3, 'tetetet', 1, 3, 0, 'open', '', 2, NULL, NULL, '4', '2018-07-30 09:47:25', '2018-07-30 09:47:25'),
(78, 'zcxzbv n', 5, '07/26/2018', 2, 'asdc', 1, 2, 0, 'open', '', 1, NULL, NULL, '45', '2018-07-30 10:00:38', '2018-07-30 10:00:38'),
(79, 'zcxzbv n', 5, '07/26/2018', 2, 'asdc', 1, 2, 0, 'open', '', 1, NULL, NULL, '45', '2018-07-30 10:04:53', '2018-07-30 10:04:53'),
(80, 'aSDAD', 3, '07/31/2018', 3, 'werewr', 1, 3, 0, 'open', '', 1, NULL, NULL, NULL, '2018-07-30 10:05:50', '2018-07-30 10:05:50'),
(81, 'qwedqwe', 3, '07/31/2018', 1, 'this is great', 1, 1, 0, 'open', '', 1, NULL, NULL, '5', '2018-07-30 10:06:49', '2018-07-30 10:06:49'),
(82, 'qwedqwe', 3, '07/31/2018', 1, 'this is great', 1, 1, 0, 'open', '', 1, NULL, NULL, '5', '2018-07-30 10:07:01', '2018-07-30 10:07:01'),
(83, 'werwer', 4, '07/24/2018', 3, 'werfet', 1, 2, 0, 'open', '', 1, NULL, NULL, '3', '2018-07-30 10:09:46', '2018-07-30 10:09:46'),
(84, 'This is new', 1, '07/26/2018', 1, 'zxcxzc', 1, 3, 0, 'open', '', 2, NULL, NULL, '5', '2018-07-31 08:45:02', '2018-07-31 08:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `task_user`
--

CREATE TABLE `task_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_user`
--

INSERT INTO `task_user` (`id`, `task_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 1, 4, NULL, NULL),
(3, 1, 6, NULL, NULL),
(4, 2, 4, NULL, NULL),
(5, 3, 4, NULL, NULL),
(6, 4, 1, NULL, NULL),
(7, 4, 3, NULL, NULL),
(8, 4, 5, NULL, NULL),
(9, 5, 5, NULL, NULL),
(10, 6, 2, NULL, NULL),
(11, 6, 4, NULL, NULL),
(12, 7, 3, NULL, NULL),
(13, 7, 6, NULL, NULL),
(14, 8, 3, NULL, NULL),
(15, 8, 6, NULL, NULL),
(16, 9, 1, NULL, NULL),
(17, 9, 2, NULL, NULL),
(18, 9, 4, NULL, NULL),
(19, 10, 1, NULL, NULL),
(20, 10, 2, NULL, NULL),
(21, 10, 4, NULL, NULL),
(22, 11, 1, NULL, NULL),
(23, 11, 2, NULL, NULL),
(24, 11, 4, NULL, NULL),
(25, 12, 1, NULL, NULL),
(26, 12, 2, NULL, NULL),
(27, 12, 4, NULL, NULL),
(28, 13, 1, NULL, NULL),
(29, 13, 2, NULL, NULL),
(30, 13, 4, NULL, NULL),
(31, 14, 3, NULL, NULL),
(32, 14, 4, NULL, NULL),
(33, 15, 3, NULL, NULL),
(34, 15, 4, NULL, NULL),
(35, 16, 1, NULL, NULL),
(36, 16, 2, NULL, NULL),
(37, 16, 3, NULL, NULL),
(38, 16, 4, NULL, NULL),
(39, 17, 1, NULL, NULL),
(40, 17, 2, NULL, NULL),
(41, 17, 3, NULL, NULL),
(42, 17, 4, NULL, NULL),
(43, 18, 1, NULL, NULL),
(44, 18, 2, NULL, NULL),
(45, 18, 3, NULL, NULL),
(46, 18, 4, NULL, NULL),
(47, 19, 1, NULL, NULL),
(48, 19, 2, NULL, NULL),
(49, 19, 3, NULL, NULL),
(50, 19, 4, NULL, NULL),
(51, 20, 1, NULL, NULL),
(52, 20, 2, NULL, NULL),
(53, 20, 3, NULL, NULL),
(54, 20, 4, NULL, NULL),
(55, 21, 1, NULL, NULL),
(56, 21, 2, NULL, NULL),
(57, 21, 3, NULL, NULL),
(58, 21, 4, NULL, NULL),
(59, 22, 1, NULL, NULL),
(60, 22, 2, NULL, NULL),
(61, 22, 3, NULL, NULL),
(62, 22, 4, NULL, NULL),
(63, 23, 4, NULL, NULL),
(64, 23, 5, NULL, NULL),
(65, 23, 6, NULL, NULL),
(66, 24, 4, NULL, NULL),
(67, 24, 5, NULL, NULL),
(68, 24, 6, NULL, NULL),
(69, 25, 4, NULL, NULL),
(70, 25, 5, NULL, NULL),
(71, 25, 6, NULL, NULL),
(72, 26, 4, NULL, NULL),
(73, 26, 5, NULL, NULL),
(74, 26, 6, NULL, NULL),
(75, 27, 4, NULL, NULL),
(76, 27, 5, NULL, NULL),
(77, 27, 6, NULL, NULL),
(78, 28, 4, NULL, NULL),
(79, 28, 5, NULL, NULL),
(80, 28, 6, NULL, NULL),
(81, 30, 2, NULL, NULL),
(82, 30, 3, NULL, NULL),
(83, 30, 5, NULL, NULL),
(84, 31, 2, NULL, NULL),
(85, 31, 3, NULL, NULL),
(86, 31, 5, NULL, NULL),
(87, 32, 2, NULL, NULL),
(88, 32, 3, NULL, NULL),
(89, 32, 5, NULL, NULL),
(90, 33, 2, NULL, NULL),
(91, 33, 3, NULL, NULL),
(92, 33, 5, NULL, NULL),
(93, 34, 2, NULL, NULL),
(94, 34, 3, NULL, NULL),
(95, 34, 5, NULL, NULL),
(96, 35, 2, NULL, NULL),
(97, 35, 3, NULL, NULL),
(98, 35, 5, NULL, NULL),
(99, 36, 2, NULL, NULL),
(100, 36, 3, NULL, NULL),
(101, 36, 5, NULL, NULL),
(102, 37, 2, NULL, NULL),
(103, 37, 3, NULL, NULL),
(104, 37, 5, NULL, NULL),
(105, 38, 2, NULL, NULL),
(106, 38, 3, NULL, NULL),
(107, 38, 5, NULL, NULL),
(108, 39, 2, NULL, NULL),
(109, 39, 3, NULL, NULL),
(110, 39, 5, NULL, NULL),
(111, 40, 2, NULL, NULL),
(112, 40, 3, NULL, NULL),
(113, 40, 5, NULL, NULL),
(114, 41, 2, NULL, NULL),
(115, 41, 3, NULL, NULL),
(116, 41, 5, NULL, NULL),
(117, 42, 2, NULL, NULL),
(118, 42, 3, NULL, NULL),
(119, 42, 5, NULL, NULL),
(120, 43, 2, NULL, NULL),
(121, 43, 3, NULL, NULL),
(122, 43, 5, NULL, NULL),
(123, 44, 2, NULL, NULL),
(124, 44, 3, NULL, NULL),
(125, 44, 5, NULL, NULL),
(126, 45, 2, NULL, NULL),
(127, 45, 3, NULL, NULL),
(128, 45, 5, NULL, NULL),
(129, 46, 2, NULL, NULL),
(130, 46, 3, NULL, NULL),
(131, 46, 5, NULL, NULL),
(132, 47, 2, NULL, NULL),
(133, 47, 3, NULL, NULL),
(134, 47, 5, NULL, NULL),
(135, 48, 2, NULL, NULL),
(136, 48, 6, NULL, NULL),
(137, 49, 2, NULL, NULL),
(138, 49, 6, NULL, NULL),
(139, 50, 2, NULL, NULL),
(140, 50, 6, NULL, NULL),
(141, 51, 2, NULL, NULL),
(142, 51, 6, NULL, NULL),
(143, 52, 1, NULL, NULL),
(144, 52, 2, NULL, NULL),
(145, 52, 3, NULL, NULL),
(146, 53, 1, NULL, NULL),
(147, 53, 2, NULL, NULL),
(148, 53, 3, NULL, NULL),
(149, 54, 1, NULL, NULL),
(150, 54, 2, NULL, NULL),
(151, 54, 3, NULL, NULL),
(152, 55, 1, NULL, NULL),
(153, 55, 2, NULL, NULL),
(154, 55, 3, NULL, NULL),
(155, 56, 4, NULL, NULL),
(156, 56, 5, NULL, NULL),
(157, 57, 4, NULL, NULL),
(158, 57, 5, NULL, NULL),
(159, 58, 4, NULL, NULL),
(160, 58, 5, NULL, NULL),
(161, 59, 1, NULL, NULL),
(162, 59, 2, NULL, NULL),
(163, 59, 3, NULL, NULL),
(164, 60, 1, NULL, NULL),
(165, 60, 2, NULL, NULL),
(166, 60, 3, NULL, NULL),
(167, 61, 1, NULL, NULL),
(168, 61, 2, NULL, NULL),
(169, 61, 3, NULL, NULL),
(170, 62, 1, NULL, NULL),
(171, 62, 2, NULL, NULL),
(172, 63, 1, NULL, NULL),
(173, 63, 2, NULL, NULL),
(174, 64, 1, NULL, NULL),
(175, 64, 2, NULL, NULL),
(176, 65, 1, NULL, NULL),
(177, 65, 2, NULL, NULL),
(178, 66, 1, NULL, NULL),
(179, 66, 2, NULL, NULL),
(180, 67, 1, NULL, NULL),
(181, 67, 2, NULL, NULL),
(182, 68, 1, NULL, NULL),
(183, 68, 2, NULL, NULL),
(184, 69, 1, NULL, NULL),
(185, 69, 2, NULL, NULL),
(186, 70, 1, NULL, NULL),
(187, 70, 2, NULL, NULL),
(188, 70, 3, NULL, NULL),
(189, 71, 1, NULL, NULL),
(190, 71, 2, NULL, NULL),
(191, 71, 3, NULL, NULL),
(192, 72, 1, NULL, NULL),
(193, 72, 2, NULL, NULL),
(194, 72, 3, NULL, NULL),
(195, 73, 1, NULL, NULL),
(196, 73, 2, NULL, NULL),
(197, 73, 3, NULL, NULL),
(198, 74, 2, NULL, NULL),
(199, 75, 3, NULL, NULL),
(200, 75, 4, NULL, NULL),
(201, 75, 5, NULL, NULL),
(202, 76, 2, NULL, NULL),
(203, 77, 6, NULL, NULL),
(204, 78, 2, NULL, NULL),
(205, 79, 2, NULL, NULL),
(206, 80, 4, NULL, NULL),
(207, 81, 3, NULL, NULL),
(208, 82, 3, NULL, NULL),
(209, 83, 4, NULL, NULL),
(210, 84, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_level_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `job_level_id`, `department_id`, `position_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'vinn', 'vinny', 'vinn@hotmail.com', '$2y$10$EWLunGM9JkQUMgGew/4EuOUHa7.PQnKglfSfe4BPkqNpApmufMsx6', 5, 1, 2, NULL, '2018-07-29 09:16:04', '2018-07-29 09:16:04'),
(2, 'janet', 'jane', 'jane@yahoo.com', '$2y$10$8F8iGJyiXgSMKx7Za7VV4eBR9e0vzgR9Ev0GAbKTHnYHAY/P6La4q', 1, 2, 1, NULL, '2018-07-29 09:16:25', '2018-07-29 09:16:25'),
(3, 'samuel', 'sam', 'sam@yahoo.com', '$2y$10$HdV5NhwJM9jD/GzaQIQkq.neHTFZSrha01nV7PmTAKOauFn4p00SK', 3, 3, 1, NULL, '2018-07-29 09:17:00', '2018-07-29 09:17:00'),
(4, 'dan', 'danny', 'dan@yahoo.com', '$2y$10$nlU/jZht9VWSGpSxs2rUhu3t0zXO4O4ZZdGRTNZYpgPHp5P8W2KtC', 4, 4, 1, NULL, '2018-07-29 09:17:33', '2018-07-29 09:17:33'),
(5, 'peter', 'petra', 'petra@yahoo.com', '$2y$10$etzdvHluO7mwq0eqttV9k.wHzTlbjaSOmG6/qBdL3623orF4dUFza', 2, 4, 1, NULL, '2018-07-29 09:18:01', '2018-07-29 09:18:01'),
(6, 'david', 'dave', 'dave@yahoo.com', '$2y$10$PaTfIlekjpdfdhZIBOQP8OWWtQtk.u1w1AOS/oBan5kkFiJUTN6vK', 3, 7, 2, NULL, '2018-07-29 09:18:46', '2018-07-29 09:18:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_task`
--
ALTER TABLE `department_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_employees`
--
ALTER TABLE `follow_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow_tasks`
--
ALTER TABLE `follow_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_levels`
--
ALTER TABLE `job_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_tasks`
--
ALTER TABLE `my_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_identities`
--
ALTER TABLE `oauth_identities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_user`
--
ALTER TABLE `task_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `department_task`
--
ALTER TABLE `department_task`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT for table `follow_employees`
--
ALTER TABLE `follow_employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `follow_tasks`
--
ALTER TABLE `follow_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `job_levels`
--
ALTER TABLE `job_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `my_tasks`
--
ALTER TABLE `my_tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_identities`
--
ALTER TABLE `oauth_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `task_user`
--
ALTER TABLE `task_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
