<?php


Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashboardController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('departments', 'DepartmentsController');
    Route::resource('joblevels', 'JobLevelController');
    Route::resource('users', 'UsersController');
    Route::resource('tasks', 'TasksController');
    Route::resource('mytasks', 'MyTasksController');
    Route::get('download/{file}', [
        'uses' => 'TasksController@download',
        'as' => 'documents.download'
    ]);
    Route::get('departmental', [
        'uses' => 'TasksController@departments',
        'as' => 'tasks.departments'
    ]);
    Route::get('depart/{id}', [
        'uses' => 'TasksController@viewDepartmentTasks',
        'as' => 'tasks.departs'
    ]);
    Route::post('reassign/{id}', [
        'uses' => 'TasksController@reassign',
        'as' => 'tasks.taskuser'
    ]);
    Route::get('employee/{id}', [
        'uses' => 'TasksController@reassignTo',
        'as' => 'tasks.employee'
    ]);
    Route::get('follow/{id}', [
        'uses' => 'TasksController@follow',
        'as' => 'follow.task'
    ]);
    Route::get('followuser/{id}', [
        'uses' => 'UsersController@followuser',
        'as' => 'follow.user'
    ]);
    Route::post('newcomment/{id}', [
        'uses' => 'MyTasksController@newComment',
        'as' => 'comments.new'
    ]);
});
Route::get('{provider}/auth', [
    'uses' => 'SocialsController@auth',
    'as' => 'social.auth'
]);
Route::get('{provider}/redirect', [
    'uses' => 'SocialsController@auth_callback',
    'as' => 'social.callback'
]);
Route::get('/home', 'HomeController@index')->name('home');
