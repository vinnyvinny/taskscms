let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/css/libs/dataTables.bootstrap.min.css',
    'resources/assets/css/libs/materialdesignicons.min.css',
    'resources/assets/css/libs/vendor.bundle.base.css',
    'resources/assets/css/libs/vendor.bundle.addons.css',
    'resources/assets/css/libs/style.css',


],'public/css/libs.css');

mix.scripts([

    'resources/assets/js/libs/jquery.dataTables.min.js',
    'resources/assets/js/libs/dataTables.bootstrap.min.js',
    'resources/assets/js/libs/vendor.bundle.addons.js',
    'resources/assets/js/libs/off-canvas.js',
    'resources/assets/js/libs/misc.js',
    'resources/assets/js/libs/dashboard.js',

],'public/js/libs.js')
