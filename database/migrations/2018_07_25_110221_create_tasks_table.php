<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('category_id')->unsigned();
            $table->string('due_date');
            $table->integer('priority_id')->unsigned();
            $table->string('description');
            $table->integer('created_by');
            $table->integer('assign_to')->unsigned();
            $table->integer('done')->default(0);
            $table->string('status')->default('open');
            $table->string('document')->nullable();
            $table->integer('access_level_id')->unsigned();
            $table->string('recur_days')->nullable();
            $table->string('recur_daily')->nullable();
            $table->string('send_email_after')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
