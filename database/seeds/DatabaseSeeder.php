<?php

use App\AccessLevel;
use App\Category;
use App\Department;
use App\JobLevel;
use App\Position;
use App\Priority;
use App\Task;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Position::truncate();
        AccessLevel::truncate();
        Category::truncate();
        Department::truncate();
        Priority::truncate();
        JobLevel::truncate();
        User::truncate();
        Task::truncate();

        $usersQuantity=50;
        $categoriesQuantity=20;
        $tasksQuantity=100;


        factory(Position::class,2)->create();
        factory(AccessLevel::class,2)->create();
        factory(Category::class,$categoriesQuantity)->create();
        factory(Department::class,4)->create();
        factory(JobLevel::class,3)->create();
        factory(Priority::class,4)->create();
        factory(User::class,$usersQuantity)->create();
        factory(Task::class,$tasksQuantity)->create();

    }
}
