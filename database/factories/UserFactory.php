<?php

use App\AccessLevel;
use App\Category;
use App\Department;
use App\JobLevel;
use App\Priority;
use App\Task;
use App\User;
use Faker\Generator as Faker;
use App\Position;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(Position::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['junior','senior']),

    ];
});

$factory->define(AccessLevel::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['public','private']),

    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,

    ];
});

$factory->define(Department::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['ict','procurement','finance','sales']),

    ];
});

$factory->define(Priority::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['super high','high','medium','low']),

    ];
});

$factory->define(JobLevel::class, function (Faker $faker) {
    return [
        'level' => $faker->randomElement(['intern','expert','consultant']),

    ];
});
$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'job_level_id' => JobLevel::all()->random()->id ,
        'department_id' => Department::all()->random()->id ,
        'position_id' => Position::all()->random()->id ,
        'password' => bcrypt('secret'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(Task::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'due_date' => $faker->date,
        'priority_id' =>Priority::all()->random()->id ,
        'category_id' => Category::all()->random()->id,
        'description' => $faker->paragraph(1),
        'access_level_id' => AccessLevel::all()->random()->id,
        'created_by' => User::all()->random()->id,
        'assign_to' => User::all()->random()->id

    ];
});

