<?php

namespace App;

use App\AccessLevel;
use App\Category;
use App\Comment;
use App\Department;
use App\FollowTask;
use App\Priority;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title', 'category_id', 'due_date', 'priority_id', 'description', 'access_level_id',
        'document','assign_to','created_by','done','status','send_email_after','recur_daily','recur_days'];

    function category()
    {
        return $this->belongsTo(Category::class);
    }

    function departments()
    {
        return $this->belongsToMany('App\Department');
    }

    function users()
    {
        return $this->belongsToMany('App\User');
    }

    function access_level()
    {
        return $this->belongsTo(AccessLevel::class);
    }


    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }
    public function employee(){
        return $this->belongsTo('App\User','assign_to');
    }
    public function comments(){
        return $this->belongsToMany(Comment::class);
    }

    public function followtasks(){
        return $this->belongsToMany(FollowTask::class);
    }

    public function getTitleAttribute($title)
    {
        return ucfirst($title);
    }
    public function getStatusAttribute($status)
    {
        return ucfirst($status);
    }
}
