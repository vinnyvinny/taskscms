<?php

namespace App;

use App\Department;
use App\JobLevel;
use App\Position;
use App\Task;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'job_level_id', 'department_id', 'position_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function job_level()
    {
        return $this->belongsTo(JobLevel::class);
    }

    function department()
    {
        return $this->belongsTo(Department::class);
    }

    function departments()
    {
        return $this->belongsToMany('App\Department');
    }


    function tasks()
    {
        return $this->belongsToMany('App\Task');
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function getFirstNameAttribute($fname)
    {
        return ucfirst($fname);
    }

    public function getLastNameAttribute($lname)
    {
        return ucfirst($lname);
    }

}
