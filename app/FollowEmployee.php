<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowEmployee extends Model
{
    protected $fillable = ['employee_id','user_id'];
}
