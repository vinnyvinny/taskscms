<?php

namespace App;
use App\Task;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    function tasks(){
        return $this->hasMany(Task::class);
    }
    public function getNameAttribute($name){
        return ucfirst($name);
    }
}
