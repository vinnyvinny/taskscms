<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name'];

    function users(){
        return $this->hasMany(User::class);
    }
    public function getNameAttribute($name){
        return ucfirst($name);
    }

    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }

}
