<?php

namespace App\Http\Controllers;

use App\AccessLevel;
use App\Category;
use App\Comment;
use App\Department;
use App\FollowTask;
use App\Priority;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use App\Notifications\NewTaskAdded;
use Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->position->name == 'Senior'):
               return view('tasks.index')->with('tasks', Task::orderBy('created_at','desc')->get());
              endif;

         return view('tasks.index')->with('tasks',Task::whereAccessLevelId(1)
                      ->orWhere('created_by',Auth::id())->orderBy('created_at','desc')
                      ->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create')->with('categories', Category::all())
            ->with('priorities', Priority::all())
            ->with('employees', User::all())
            ->with('departments', Department::all())
            ->with('access_levels', AccessLevel::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,
            ['title' => 'required',
                'due_date' => 'required',
                'priority_id' => 'required',
                'category_id' => 'required',
                'user_id' => 'required',
                'description' => 'required',
                'access_level_id' => 'required',
                'department_id' => 'required',
                'assign_to' => 'required',


            ]
        );

        $document_name = '';
        if ($request->hasFile('document')) {
            $document = $request->file('document');
            $document_name = time() . $document->getClientOriginalName();
            $document->move('uploads/documents', $document_name);

        }

        $task = Task::create([
            'title' => $request->get('title'),
            'due_date' => $request->get('due_date'),
            'priority_id' => $request->get('priority_id'),
            'category_id' => $request->get('category_id'),
            'description' => $request->get('description'),
            'access_level_id' => $request->get('access_level_id'),
            'created_by' => Auth::id(),
            'assign_to' => $request->get('assign_to'),
            'document' => $document_name,
            'recur_days' => $request->get('recur_days'),
            'recur_daily' => $request->get('recur_days') ? '' : $request->get('recur_daily'),
            'send_email_after' => $request->get('send_email_after'),

        ]);
       
        $task->departments()->attach($request->get('department_id'));
        $task->users()->attach($request->get('user_id'));


        $user_group = array();

        foreach($request->get('user_id') as $ids):
            array_push($user_group,User::find($ids));
        endforeach;

         foreach($request->get('department_id') as $d_ids):
             array_push($user_group,User::whereDepartmentId($d_ids)->first());
         endforeach;

        Notification::send($user_group, new NewTaskAdded($task));
                    
        Session::flash('success', 'You have successfully added a task');
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('tasks.show')->with('task', Task::findOrfail($id))
            ->with('categories', Category::all())
            ->with('priorities', Priority::all())
            ->with('employees', User::all())
            ->with('departments', Department::all())
            ->with('access_levels', AccessLevel::all())
            ->with('comments',Comment::orderBy('created_at','desc')->whereTaskId($id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('tasks.edit')->with('task', Task::findOrfail($id))
            ->with('categories', Category::all())
            ->with('priorities', Priority::all())
            ->with('employees', User::all())
            ->with('departments', Department::all())
            ->with('access_levels', AccessLevel::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            ['title' => 'required',
                'due_date' => 'required',
                'priority_id' => 'required',
                'category_id' => 'required',
                'user_id' => 'required',
                'description' => 'required',
                'access_level_id' => 'required',
                'department_id' => 'required',
                'assign_to' => 'required',

            ]

        );
        $task = Task::findOrFail($id);

        if ($request->hasFile('document')) {
            $document = $request->get('document');
            $document_name = time() . $document->getClientOriginalName();
            $document->move('uploads/documents', $document_name);
            $task->document = $document_name;

        }
        $task->title = $request->get('title');
        $task->due_date = $request->get('due_date');
        $task->priority_id = $request->get('priority_id');
        $task->category_id = $request->get('category_id');
        $task->description = $request->get('description');
        $task->access_level_id = $request->get('access_level_id');
        $task->assign_to = $request->get('assign_to');
        $task->save();

        $task->departments()->sync($request->get('department_id'));
        $task->users()->sync($request->get('user_id'));

        Session::flash('success', 'You successfully updated a task');
        return redirect()->route('tasks.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::destroy($id);

        Session::flash('success', 'You successfully deleted a task');
        return redirect()->back();
    }

    public function download($file_name)
    {
        $file_path = public_path('uploads/documents/' . $file_name);
        return response()->download($file_path);
    }

    public function departments()
    {

        return view('tasks.departments')->with('departments',Department::all());
    }

    public function  viewDepartmentTasks($id){

       return view('tasks.department_tasks')->with('departments',Department::findOrFail($id));

    }
    public function reassignTo($id){

       return view('tasks.reassign')->with('task',Task::findOrFail($id))->with('users',User::all());
    }
    public function reassign()
    {
     $this->validate(request(),['assign_to' => 'required']);

     $task = Task::findOrFail(request()->get('id'));
     $task->assign_to = request()->get('assign_to');
     $task->save();

     Session::flash('success','Task succesfully updated.');
     return redirect()->route('tasks.index');

    }

    public function follow($id)
    {

     FollowTask::create([
         'task_id' => $id,
         'user_id' => Auth::id()
     ]);

     Session::flash('success','Successfully followed a task.');
     return redirect()->route('tasks.index');
    }


}
