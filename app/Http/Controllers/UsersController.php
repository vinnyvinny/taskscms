<?php

namespace App\Http\Controllers;

use App\Department;
use App\FollowEmployee;
use App\JobLevel;
use App\Position;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index')->with('users', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create')->with('users', User::all())
            ->with('joblevels', JobLevel::all())
            ->with('positions', Position::all())
            ->with('departments', Department::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,
            ['first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'job_level_id' => 'required',
                'department_id' => 'required',
                'position_id' => 'required'
            ]);

        User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'password' => bcrypt($request->get('password')),
            'email' => $request->get('email'),
            'job_level_id' => $request->get('job_level_id'),
            'department_id' => $request->get('department_id'),
            'position_id' => $request->get('position_id')
        ]);

        Session::flash('success', 'You successfully created a user');
        return redirect()->route('users.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('users.show')->with('user', User::findOrFail($id))
            ->with('joblevels', JobLevel::all())
            ->with('positions', Position::all())
            ->with('departments', Department::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('users.edit')->with('user', User::findOrFail($id))
                                       ->with('joblevels', JobLevel::all())
                                       ->with('positions', Position::all())
                                       ->with('departments', Department::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request,
            ['first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email,' . $user->id,
                'job_level_id' => 'required',
                'department_id' => 'required',
                'position_id' => 'required'
            ]);

        $user = User::findOrFail($id);

        if ($request->has('password')) {
            $user->password = bcrypt($request->get('password'));

        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->job_level_id = $request->get('job_level_id');
        $user->department_id = $request->get('department_id');
        $user->department_id = $request->get('position_id');
        $user->save();

        Session::flash('success', 'You successfully updated a user');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('success', 'You succesfully deleted a user');
        return redirect()->back();
    }

    public function followuser($id)
    {
        FollowEmployee::create([
            'employee_id' => $id,
            'user_id' => Auth::id()
        ]);

        Session::flash('success','Successfully followed an employee.');
        return redirect()->route('users.index');
    }

}
