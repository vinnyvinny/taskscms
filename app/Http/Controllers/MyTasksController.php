<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MyTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('mytasks.index')->with('tasks', Task::whereAssignTo(Auth::id())->orderBy('created_at', 'desc')->get());
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('mytasks.edit')->with('task', Task::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'done' => 'required',
            'status' => 'required',
            'comments' => 'required'
        ]);
        $task = Task::findorFail($id);

        $task->done = $request->get('done');
        $task->status = $request->get('status');
        $task->save();

        Comment::create([
            'description' => $request->get('comments'),
            'created_by' => Auth::id(),
            'task_id' => $id
        ]);

        Session::flash('success', 'Task progress updated succesfully');
        return redirect()->route('mytasks.index');
    }

    public function newComment($id)
    {

        $this->validate(request(), [
            'description' => 'required',

        ]);

        Comment::create([
            'description' => request()->get('description'),
            'created_by' => Auth::id(),
            'task_id' => $id
        ]);
        Session::flash('success', 'New comment added');
        return redirect('/tasks/' . $id);
    }

}
