<?php

namespace App\Http\Controllers;
use App\JobLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('joblevels.index')->with('joblevels',JobLevel::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('joblevels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        ['level' => 'required']
        );
        JobLevel::create($request->all());
        Session::flash('success','You successfully created a job level');
        return redirect()->route('joblevels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('joblevels.edit')->with('joblevel',JobLevel::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            ['level' => 'required']
            );
        $joblevel = JobLevel::findOrFail($id);
        $joblevel->level = $request->level;
         $joblevel->save();
         
        Session::flash('success','You successfully updated a job level');
        return redirect()->route('joblevels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JobLevel::destroy($id);

        Session::flash('success','You successfully deleted');
        return redirect()->back();
    }
}
