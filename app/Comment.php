<?php

namespace App;
use App\Task;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['description','created_by','task_id'];

    public function task(){
        return $this->belongsTo(Task::class);
    }

    public function creator(){
        return $this->belongsTo(User::class,'created_by');
    }
}
