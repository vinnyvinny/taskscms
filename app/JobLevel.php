<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class JobLevel extends Model
{
    protected $fillable = ['level'];

    function users(){
        return $this->hasMany(User::class);
    }
    public function getLevelAttribute($level){
        return ucfirst($level);
    }
}
