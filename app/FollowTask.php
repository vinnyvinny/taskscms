<?php

namespace App;
use App\Task;
use Illuminate\Database\Eloquent\Model;

class FollowTask extends Model
{
    protected $fillable = ['user_id','task_id'];
public function task(){
    return $this->belongsTo(Task::class);
}
}
