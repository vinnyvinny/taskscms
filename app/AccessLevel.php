<?php

namespace App;
use App\Task;
use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    protected $fillable = ['name'];

function tasks(){
    return $this->belongsToMany(Task::class);
}
    public function getNameAttribute($name){
        return ucfirst($name);
    }
}
