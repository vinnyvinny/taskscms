<?php

namespace App\Observer;

use App\Task;
use App\Notifications\NewTaskAdded;
use App\User;
use Notification;

class TaskObserver
{
    public function creating()
    {

     $users = User::find(2);

        $users->notify(new NewTaskAdded("this message"));
    }
}
