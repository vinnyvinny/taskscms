@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">New Department</div>
                <div class="panel panel-body">
                    <form action="{{route('departments.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" style="width: 50%">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus-circle"> Create Department</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()