@extends('layouts.layout')

@section('content')
    <a href="{{route('departments.create')}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus-circle">Add New Department</i></a>


    <table class="table table-striped table-bordered" id="table">

        <thead>
        <tr>

            <th class="text-center">#</th>
            <th class="text-center">Name</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($departments as $department)
            <tr>
                <td>{{$department->id}}</td>
                <td>{{$department->name}}</td>
                <td class="text-center">
                    <a href="{{route('departments.edit',['id' => $department->id])}}" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{route('departments.destroy',['category' => $department->id])}}" method="post" style="display: inline-block">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i></button>

                    </form>

                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
