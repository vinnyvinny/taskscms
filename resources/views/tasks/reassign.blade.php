@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">Reassign Task</div>
                <div class="panel panel-body">
                    <form action="{{route('tasks.taskuser',['id' => $task->id])}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$task->id}}" name="id">
                        <div class="form-group">
                            <label for="assign_to">Reassign To</label>
                            <select name="assign_to" id="assign_to" class="form-control">
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info" ><i class="fa fa-plus-circle"> Update Task</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()
@section('scripts')
    <script>
        $('#assign_to').select2();
    </script>
@stop

