@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel panel-heading">Edit Task</div>

                <div class="panel panel-body">

                    <form action="{{route('tasks.update',['id' => $task->id])}}" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="col-md-6">


                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title"
                                           placeholder="Title" value="{{$task->title}}">
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>

                                    <select name="category_id" id="category_id" class="form-control">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{$task->category->id == $category->id ? 'selected="selected"' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="priority">Priority</label>

                                    <select name="priority_id" id="priority_id" class="form-control">
                                        @foreach($priorities as $priority)
                                            <option value="{{$priority->id}}" {{$task->priority->id == $priority->id ? 'selected="selected"' : ''}}>{{$priority->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="due_date">Due Date</label>
                                    <input type="text" class="form-control" id="due_date" name="due_date"
                                           value="{{$task->due_date}}">
                                </div>


                                <div class="form-group">
                                    <label for="file">Attach File</label>
                                    <input type="file" class="form-control" name="document">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="department">Tagged Department(s)</label>


                                        <select name="department_id[]" id="department_id" class="form-control"
                                                multiple="multiple">
                                            @foreach ($task->departments as $dept)
                                                @foreach($departments as $department)
                                                    <option value="{{$department->id}}" {{$dept->id == $department->id ? 'selected="selected"' : ''}}>{{$department->name}}</option>
                                                @endforeach
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_id">Tagged Employee(s)</label>
                                        <select name="user_id[]" id="user_id" class="form-control" multiple="multiple">
                                            @foreach($task->users as $user)
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}" {{$user->id == $employee->id ? 'selected="selected"' : ''}}>{{$employee->first_name}}</option>
                                            @endforeach
                                                @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="access level">Access Level</label>

                                    <select name="access_level_id" id="access_level_id" class="form-control input-sm">
                                        @foreach($access_levels as $level)
                                            <option value="{{$level->id}}" {{$task->access_level->id == $level->id ? 'selected="selected"' : ''}}>{{$level->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="access level">Assigned To</label>
                                       <select name="assign_to" id="assign_to" class="form-control input-sm">

                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}" {{$task->employee->id == $emp->id ? 'selected="selected"' : ''}}>{{$emp->first_name .' '.$emp->last_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" cols="6" rows="6"
                                              class="form-control">{{$task->description}}</textarea>

                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <button class="btn btn-success" type="submit" style="margin-top: -20px"><i class="fa fa-tasks"> Update Task</i>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
