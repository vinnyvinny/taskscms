@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel panel-heading">New Task</div>

                <div class="panel panel-body">

                    <form action="{{route('tasks.store')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">


                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="priority">Priority</label>
                                    <select name="priority_id" id="priority_id" class="form-control">
                                        @foreach($priorities as $priority)
                                            <option value="{{$priority->id}}">{{$priority->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="due_date">Due Date</label>
                                    <input type="text" class="form-control" id="due_date" name="due_date">
                                </div>

                                <div class="form-group">
                                    <label for="file">Attach File</label>
                                    <input type="file" class="form-control" name="document">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="repetitive">
                                        Repetitive Task ?
                                    </label>
                                </div>
                                <div class="showRecur" style="display: none">
                                    <div class="form-group">
                                        <label for="title">Recurrence Days</label>
                                        <select name="recur_days" id="recur_days" class="form-control">
                                            <option value="">Choose a day</option>
                                            <option value="mondays">Mondays</option>
                                            <option value="tuesdays">Tuesdays</option>
                                            <option value="wednesdays">Wednesdays</option>
                                            <option value="thursdays">Thursdays</option>
                                            <option value="fridays">Fridays</option>
                                            <option value="saturdays">Saturdays</option>
                                            <option value="sundays">Sundays</option>
                                        </select>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="recur_daily" id="recur_daily" value="1">
                                            Recur Daily ?
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group showRecurdaily">
                                    <label for="send_email_after">Send Email After(days)</label>
                                    <input type="number" class="form-control" name="send_email_after" placeholder="0">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="department">Tag Department(s)</label>
                                        <select name="department_id[]" id="department_id" class="form-control" multiple="multiple">
                                            @foreach($departments as $department)
                                                <option value="{{$department->id}}">{{$department->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assign_to">Tag Employee(s)</label>
                                        <select name="user_id[]" id="user_id" class="form-control" multiple="multiple">
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->first_name .' '.$employee->last_name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="access level">Access Level</label>
                                    <select name="access_level_id" id="access_level_id" class="form-control input-sm">
                                        @foreach($access_levels as $level)
                                            <option value="{{$level->id}}">{{$level->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="access level">Assign To</label>
                                    <select name="assign_to" id="assign_to" class="form-control input-sm">
                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}">{{$emp->first_name .' '.$emp->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" cols="6" rows="6" class="form-control"></textarea>

                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-tasks"> Create Task</i></button>
                                </div>
                            </div>


                        </div>




                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $(function () {
            $('#repetitive').click(function () {
              if ($(this).is(":checked"))  {
                  $('.showRecur').show();
                  $('.showRecurdaily').hide();
              }
              else {
                  $('.showRecur').hide();
                  $('.showRecurdaily').show();
              }
            });
        });

    </script>
    @stop
