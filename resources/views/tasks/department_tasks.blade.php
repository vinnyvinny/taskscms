@extends('layouts.layout')

@section('content')

    <a href="{{route('tasks.create')}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-user-plus">Add New
            Task</i></a>

    <table class="table table-striped table-bordered" id="table">

        <thead>
        <tr>

            <th class="text-center">#</th>
            <th class="text-center">Title</th>
            <th class="text-center">Category</th>
            <th class="text-center">Priority</th>
            <th class="text-center">Done %</th>
            <th class="text-center">Status</th>
            <th class="text-center">Due Date</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($departments->tasks()->whereAccessLevelId(1)->orderBy('created_at','desc')->get() as $task)
            <tr>
                <td>{{$task->id}}</td>
                <td>{{$task->title}}</td>
                <td>{{$task->category->name}}</td>
                <td>{{$task->priority->name}}</td>
                <td>{{$task->done}}</td>
                <td>{{$task->status}}</td>
                <td>{{$task->due_date}}</td>
                <td class="text-center">
                    @if(Auth::user()->id == $task->created_by || Auth::user()->position->name =='Senior')
                        <a href="{{route('tasks.edit',['id' => $task->id])}}" class="btn btn-success btn-xs"><i
                                    class="fa fa-pencil-square-o"></i></a>
                    @endif
                    <a href="{{route('tasks.show',['id' => $task->id])}}" class="btn btn-info btn-xs"><i
                                class="fa fa-eye"></i></a>
                    @if(Auth::user()->id == $task->created_by || Auth::user()->position->name =='Senior')
                        <form action="{{route('tasks.destroy',['id' => $task->id])}}" method="post"
                              style="display: inline-block">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i></button>

                        </form>
                    @endif

                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
