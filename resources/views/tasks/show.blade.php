@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel panel-heading">View  Task
                     <a href="{{route('follow.task',['id' => $task->id])}}" class="href btn btn-success btn-xs pull-right"><i class="fa fa-user"> Follow</i></a>
                </div>

                <div class="panel panel-body">

                    <form action="{{route('tasks.update',['id' => $task->id])}}" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="row">
                            <div class="col-md-6">


                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title"
                                           placeholder="Title" value="{{$task->title}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>

                                    <select name="category_id" id="cat_id" class="form-control" disabled>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{$task->category->id == $category->id ? 'selected="selected"' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="priority">Priority</label>

                                    <select name="priority_id" id="priority_id" class="form-control" disabled>
                                        @foreach($priorities as $priority)
                                            <option value="{{$priority->id}}" {{$task->priority->id == $priority->id ? 'selected="selected"' : ''}}>{{$priority->name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label for="due_date">Due Date</label>
                                    <input type="text" class="form-control" id="due_date" name="due_date"
                                           value="{{$task->due_date}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="assigned_to">Assigned To</label>
                                    <select name="assign_to" id="assign_to" class="form-control input-sm" disabled>

                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}" {{$task->employee->id == $emp->id ? 'selected="selected"' : ''}}>{{$emp->first_name .' '.$emp->last_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <input type="text" class="form-control" value="{{$task->status}}" disabled>
                                </div>
                                @if($task->document)
                                <div class="form-group">

                                    <a href="{{route('documents.download',['file' =>$task->document])}}" class="btn btn-info btn-xs"><i class="fa fa-download">Dowload file</i></a>
                                </div>
                                    @endif
                            </div>

                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="department">Tagged Department(s)</label>

                                          <ul class="list-group">
                                              @foreach ($task->departments as $dept)
                                              <li class="list-group-item">
                                                  {{$dept->name}}
                                              </li>
                                              @endforeach
                                          </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="user_id">Tagged Employee(s)</label>
                                        <ul class="list-group">
                                            @foreach($task->users as $user)
                                            <li class="list-group-item">
                                               {{$user->first_name .' '. $user->last_name}}
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="access level" style="margin-top: 40px">Access Level</label>
                                    <select name="access_level_id" id="access_level_id" class="form-control input-sm" disabled>
                                        @foreach($access_levels as $level)
                                            <option value="{{$level->id}}" {{$task->access_level->id == $level->id ? 'selected="selected"' : ''}}>{{$level->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="done">% Done</label>
                                    <input type="text" class="form-control" value="{{$task->done}}%" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" id="description" cols="6" rows="6"
                                              class="form-control" disabled>{{$task->description}}</textarea>

                                </div>

                            </div>

                        </div>

                    </form>
                </div>
            </div>

                @if(Auth::user()->position->name == 'Senior')
            <div class="panel panel-default">
                <div class="panel panel-heading">Add Comment</div>
                <div class="panel panel-body">
                    <form action="{{route('comments.new',['id' => $task->id])}}" method="POST">
                        {{csrf_field()}}
                        <textarea name="description" id="description" cols="6" rows="6" class="form-control"></textarea>

                        <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-comments"></i> Post Comment</button>
                    </form>
                </div>
            </div>
            @endif

            @if(count($comments) > 0)
            <div class="panel panel-default">
                <h3>Comments</h3>
                  @foreach($comments as $comment)
                <div class="panel panel-heading"><strong>{{$comment->creator->first_name}}</strong> <span class="text-muted">{{$comment->created_at->diffForHumans()}}</span></div>
              <div class="panel panel-body">
                  {{$comment->description}}
              </div>
                      @endforeach
            </div>
                @endif
            </div>

        </div>


@endsection
