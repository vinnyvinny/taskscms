@extends('layouts.layout')

@section('content')
    <a href="" class="btn btn-primary btn-sm text-center" style="margin-left: 25%"><i class="fa fa-eye">All Departments</i></a>


    <table class="table table-striped table-bordered" id="table">

        <thead>
        <tr>

            <th class="text-center">#</th>
            <th class="text-center">Name</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($departments as $department)
            <tr>
                <td>{{$department->id}}</td>
                <td>{{$department->name}}</td>
                <td class="text-center">
                    <a href="{{route('tasks.departs',['id' => $department->id])}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>

                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
