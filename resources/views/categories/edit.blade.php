@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">Edit Category</div>
                <div class="panel panel-body">
                    <form action="{{route('categories.update',['id' => $category->id])}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" style="width: 50%" value="{{$category->name}}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus-circle"> Update Category</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()