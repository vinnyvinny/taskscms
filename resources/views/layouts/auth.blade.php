<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Taskscms Login</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/libs.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}">

    @yield('styles')



</head>
<body id="auth">
@yield('content')
   @yield('footer')
   @include('layouts.footer')

   <script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/libs.js')}}"></script>

</body>
</html>
