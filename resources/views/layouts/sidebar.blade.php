<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">

                <button class="btn btn-success btn-block">New Project
                    <i class="mdi mdi-plus"></i>
                </button>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}">
                <i class="menu-icon mdi mdi-television"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Tasks</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapsed" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('tasks.index')}}">All Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('mytasks.index')}}">My Tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('tasks.departments')}}">Filter By Department</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('categories.index')}}">
                <i class="menu-icon mdi mdi-backup-restore"></i>
                <span class="menu-title">Categories</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('departments.index')}}">
                <i class="menu-icon mdi mdi-chart-line"></i>
                <span class="menu-title">Departments</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('users.index')}}">
                <i class="menu-icon mdi mdi-sticker"></i>
                <span class="menu-title">Employees</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-settings" aria-expanded="false" aria-controls="ui-settings">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Settings</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapsed" id="ui-settings">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('joblevels.index')}}">Job Levels</a>
                    </li>

                </ul>
            </div>
        </li>

    </ul>
</nav>
