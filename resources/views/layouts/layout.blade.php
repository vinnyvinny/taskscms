<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Taskscms</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/libs.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}">

    @yield('styles')



</head>
<body id="app" class="app-app">

   <div class="container-scroller">
     @include('layouts.header')
     <div class="container-fluid page-body-wrapper">
         @include('layouts.sidebar')

     <main>  <div class="container">
             @include('errors.errors')
             @yield('content')
         </div>
     </main>

     </div>

   </div>
   @yield('footer')
   @include('layouts.footer')

   <script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/libs.js')}}"></script>
 <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
   <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>

  <script src="{{asset('js/toastr.min.js')}}"></script>
   <script>
       @if(Session::has('success'))
       toastr.success("{{Session::get('success')}}")
       @endif

   </script>

   <script>
       $(document).ready(function() {
           $('#table').DataTable();

           $('#department_id,#user_id').multiselect({
               enableFiltering: true
           });

           $("#category_id").select2();
           $('#due_date').datepicker();
           $('.collapse').collapse();

       } );

   </script>

   @yield('scripts')
</body>
</html>
