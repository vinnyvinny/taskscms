@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">Edit Job Level</div>
                <div class="panel panel-body">
                    <form action="{{route('joblevels.update',['id' => $joblevel->id])}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="form-group">
                            <label for="level">Level</label>
                            <input type="text" class="form-control" name="level" placeholder="Level" style="width: 50%" value="{{$joblevel->level}}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus-circle"> Update Job Level</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()