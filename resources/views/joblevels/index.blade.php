@extends('layouts.layout')

@section('content')
    <a href="{{route('joblevels.create')}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus-circle">Add New Job Level</i></a>


    <table class="table table-striped table-bordered" id="table">

        <thead>
        <tr>

            <th class="text-center">#</th>
            <th class="text-center">Level</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($joblevels as $joblevel)
            <tr>
                <td>{{$joblevel->id}}</td>
                <td>{{$joblevel->level}}</td>
                <td class="text-center">
                    <a href="{{route('joblevels.edit',['id' => $joblevel->id])}}" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                    <form action="{{route('joblevels.destroy',['id' => $joblevel->id])}}" method="post" style="display: inline-block">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i></button>

                    </form>

                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
