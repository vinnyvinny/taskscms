@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel panel-heading">View User
                    <a href="{{route('follow.user',['id' => $user->id])}}" class="href btn btn-success btn-xs pull-right"><i class="fa fa-user"> Follow</i></a>
                </div>

                <div class="panel panel-body">

                    <form action="{{route('users.update',['id' => $user->id])}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" name="first_name" placeholder="First Name"  value="{{$user->first_name}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{$user->last_name}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email"  value="{{$user->email}}" disabled>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="job level">Job Level</label>

                                <select name="job_level_id" id="job_level_id" class="form-control" disabled>
                                    @foreach($joblevels as $joblevel)
                                        <option value="{{$joblevel->id}}" {{$user->job_level->id == $joblevel->id ? 'selected="selected"' : ''}}>{{$joblevel->level}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="department">Department</label>
                                <select name="department_id" id="department" class="form-control" disabled>
                                    @foreach($departments as $department)
                                        <option value="{{$department->id}}" {{$user->department->id == $department->id ? 'selected="selected"' : ''}}>{{$department->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <select name="position_id" id="position_id" class="form-control" disabled>
                                    @foreach($positions as $position)
                                        <option value="{{$position->id}}" {{$user->position->id == $position->id ? 'selected="selected"' :''}}>{{$position->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()
