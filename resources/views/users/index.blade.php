@extends('layouts.layout')

@section('content')

    <a href="{{route('users.create')}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-user-plus">Add New
            User</i></a>


    <table class="table table-striped table-bordered" id="table">

        <thead>
        <tr>

            <th class="text-center">#</th>
            <th class="text-center">First Name</th>
            <th class="text-center">Last Name</th>
            <th class="text-center">Email</th>
            <th class="text-center">Job Level</th>
            <th class="text-center">Department</th>
            <th class="text-center">Position</th>
            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->first_name}}</td>
                <td>{{$user->last_name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->job_level->level}}</td>
                <td>{{$user->department->name}}</td>
                <td>{{$user->position->name}}</td>
                <td class="text-center">
                    <a href="{{route('users.edit',['id' => $user->id])}}" class="btn btn-success btn-xs"><i
                                class="fa fa-pencil-square-o"></i></a>
                    <a href="{{route('users.show',['id' => $user->id])}}" class="btn btn-info btn-xs"><i
                                class="fa fa-eye"></i></a>
                    <form action="{{route('users.destroy',['user' => $user->id])}}" method="post"
                          style="display: inline-block">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o"></i></button>

                    </form>

                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection
