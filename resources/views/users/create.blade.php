@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel panel-heading">New User</div>

                <div class="panel panel-body">

                    <form action="{{route('users.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="col-md-6">


                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                        </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                            </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                        </div>

                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="job level">Job Level</label>
                            <select name="job_level_id" id="job_level_id" class="form-control input-sm">
                                @foreach($joblevels as $joblevel)
                                    <option value="{{$joblevel->id}}">{{$joblevel->level}}</option>
                                    @endforeach
                            </select>
                             </div>
                        <div class="form-group">
                            <label for="department">Department</label>
                            <select name="department_id" id="department" class="form-control">
                                @foreach($departments as $department)
                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                    @endforeach
                            </select>

                        </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <select name="position_id" id="position_id" class="form-control">
                                    @foreach($positions as $position)
                                        <option value="{{$position->id}}">{{$position->name}}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>


                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" style="margin-top: 25px"><i class="fa fa-user-plus"> Create User</i></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection()

@section('scripts')
    <script>
        $('#department').select();
    </script>
    @stop