@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">Update Task Progress</div>
                <div class="panel panel-body">
                    <form action="{{route('mytasks.update',['id' => $task->id])}}" method="post">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="form-group">
                            <label for="done">% done</label>
                            <select name="done" id="done" class="form-control">
                                <option value="0">0% done</option>
                                <option value="10">10% done</option>
                                <option value="25">25% done</option>
                                <option value="50">50% done</option>
                                <option value="75">75% done</option>
                                <option value="100">100% done</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="pending">Pending</option>
                                <option value="started">Started</option>
                                <option value="completed">Completed</option>
                            </select>
                        </div>
                           <div class="form-group">
                               <label for="comments">Comments</label>
                               <textarea name="comments" id="comments" cols="6" rows="6" class="form-control"></textarea>
                           </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-plus-circle"> Update Task Progress</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection()